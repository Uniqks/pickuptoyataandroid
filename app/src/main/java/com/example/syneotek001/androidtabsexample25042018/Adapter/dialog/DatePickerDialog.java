package com.example.syneotek001.androidtabsexample25042018.Adapter.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;


import com.example.syneotek001.androidtabsexample25042018.R;

import java.util.Calendar;

public class DatePickerDialog extends AppCompatDialogFragment {
    Calendar calendar = Calendar.getInstance();
    View view;

    public Button btnSave;
    public DatePicker datePicker;

    class onDialogClick implements OnClickListener {
        onDialogClick() {
        }

        public void onClick(DialogInterface dialog, int whichButton) {
            dialog.dismiss();
        }
    }

    class DialogNevigationClick implements OnClickListener {
        DialogNevigationClick() {
        }

        public void onClick(DialogInterface dialog, int whichButton) {
            int dayOfMonth = datePicker.getDayOfMonth();
            int month = datePicker.getMonth();
            DatePickerDialog.this.getTargetFragment().onActivityResult(DatePickerDialog.this.getTargetRequestCode(), -1, new Intent().putExtra("dayOfMonth", dayOfMonth).putExtra("month", month).putExtra("year", datePicker.getYear()));
            dialog.dismiss();
        }
    }

    class C05733 implements View.OnClickListener {
        C05733() {
        }

        public void onClick(View view) {
            int dayOfMonth = datePicker.getDayOfMonth();
            DatePickerDialog.this.getTargetFragment().onActivityResult(DatePickerDialog.this.getTargetRequestCode(), -1, new Intent().putExtra("dayOfMonth", dayOfMonth).putExtra("month", datePicker.getMonth()).putExtra("year", datePicker.getYear()));
            DatePickerDialog.this.dismiss();
        }
    }

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        if (this.view == null) {
            View view = inflater.inflate(R.layout.dialog_date_picker, null, false);
            btnSave = view.findViewById(R.id.btnSave);
            datePicker = view.findViewById(R.id.datePicker);
//            this.mBinding = (DialogDatePickerBinding) DataBindingUtil.inflate(inflater, R.layout.dialog_date_picker, null, false);
//            this.view = this.mBinding.getRoot();
            prepareLayout(getArguments());
        }
        Builder alertBuilder = new Builder(getActivity()).setPositiveButton(getActivity().getResources().getString(R.string.save), new DialogNevigationClick()).setNegativeButton(getActivity().getResources().getString(R.string.cancel), new onDialogClick());
        alertBuilder.setView(this.view);
        AlertDialog dialog = alertBuilder.create();
        dialog.show();
        return dialog;
    }

    private void prepareLayout(Bundle bundle) {
        int dayOfMonth;
        int year;
        int month;
        if (bundle != null) {
            dayOfMonth = bundle.getInt("dayOfMonth");
            year = bundle.getInt("year");
            month = bundle.getInt("month");
        } else {
            dayOfMonth = this.calendar.get(Calendar.DAY_OF_MONTH);
            year = this.calendar.get(Calendar.YEAR);
            month = this.calendar.get(Calendar.MONTH);
        }
        if (VERSION.SDK_INT >= 23) {
            datePicker.setMinDate((long) dayOfMonth);
        } else {
            datePicker.setMinDate((long) dayOfMonth);
        }
    }

    private void setClickEvent() {
        btnSave.setOnClickListener(new C05733());
    }
}
