package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnExpandableRecyclerViewChildItemClickListener;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;


public class NavigationDrawerAdapter extends ExpandableRecyclerViewAdapter<NavigatoinDrawerParentViewHolder, NavigationDrawerChildViewHolder> {
    private OnExpandableRecyclerViewChildItemClickListener mCallBack;

    public NavigationDrawerAdapter(List<? extends ExpandableGroup> groups, OnExpandableRecyclerViewChildItemClickListener listener) {
        super(groups);
        this.mCallBack = listener;
    }

    public NavigatoinDrawerParentViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        return new NavigatoinDrawerParentViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_navigation_drawer_parent, parent, false));
    }

    public NavigationDrawerChildViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        return new NavigationDrawerChildViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_navigation_drawer_child, parent, false));
    }

    public void onBindChildViewHolder(NavigationDrawerChildViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        holder.setArtistName(((NavigationDrawerChildModel) ((NavigationDrawerParentModel) group).getItems().get(childIndex)).getName(), this.mCallBack);
    }

    public void onBindGroupViewHolder(NavigatoinDrawerParentViewHolder holder, int flatPosition, ExpandableGroup group) {
        holder.setGenreTitle(group, this.mCallBack);
    }
}
