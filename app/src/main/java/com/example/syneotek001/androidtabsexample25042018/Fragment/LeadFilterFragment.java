package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;


public class LeadFilterFragment extends Fragment implements OnBackPressed {
    public ImageView ivMenu,ivLabelAll,ivLabelClosed,ivLabelCold,ivLabelDead,ivLabelHot,ivLabelWarm;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        View view=inflater.inflate(R.layout.fragment_filter_lead, container, false);
        ivLabelAll=view.findViewById(R.id.ivLabelAll);
        ivLabelClosed=view.findViewById(R.id.ivLabelClosed);
        ivLabelCold=view.findViewById(R.id.ivLabelCold);
        ivLabelDead=view.findViewById(R.id.ivLabelDead);
        ivLabelHot=view.findViewById(R.id.ivLabelHot);
        ivLabelWarm=view.findViewById(R.id.ivLabelWarm);
        ivMenu=view.findViewById(R.id.ivMenu);

        ivMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        return view;
    }

    @Override
    public void onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

  /*  @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (this.view == null) {
            this.mBinding = (FragmentFilterLeadBinding) DataBindingUtil.inflate(inflater, R.layout.fragment_filter_lead, container, false);
            this.view = this.mBinding.getRoot();
            setupToolBarWithBackArrow(this.mBinding.toolbar.toolbar, this.mContext.getResources().getString(R.string.filter));
        }
        setHasOptionsMenu(true);
        return this.view;
    }*/

}
