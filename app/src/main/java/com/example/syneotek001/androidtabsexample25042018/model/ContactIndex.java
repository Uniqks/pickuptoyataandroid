package com.example.syneotek001.androidtabsexample25042018.model;

/**
 * Created by bluegenie-24 on 19/7/18.
 */

public class ContactIndex {
    String index_name;
    String index_value;
    boolean is_selected;
    public ContactIndex(String index_name,String index_value,boolean is_selected){
        this.index_name = index_name;
        this.index_value = index_value;
        this.is_selected = is_selected;
    }
    public String getIndex_name() {
        return index_name;
    }

    public void setIndex_name(String index_name) {
        this.index_name = index_name;
    }

    public String getIndex_value() {
        return index_value;
    }

    public void setIndex_value(String index_value) {
        this.index_value = index_value;
    }

    public boolean isIs_selected() {
        return is_selected;
    }

    public void setIs_selected(boolean is_selected) {
        this.is_selected = is_selected;
    }
}
