package com.example.syneotek001.androidtabsexample25042018.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class TimeStamp {
    private static final String DATE_FORMAT_SHOW = "dd-MM-yyyy";
    private static final int DAY_MILLIS = 86400000;
    private static final int HOUR_MILLIS = 3600000;
    private static final int MINUTE_MILLIS = 60000;
    private static final int SECOND_MILLIS = 1000;
    private static final String TIME_FORMAT_SHOW = "dd-MM-yyyy hh:mm aa";
    private static TimeZone tz = TimeZone.getDefault();

    public static long formatToSeconds(String value, String format) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.ENGLISH);
            sdf.setTimeZone(tz);
            return TimeUnit.MILLISECONDS.toSeconds(sdf.parse(value).getTime());
        } catch (ParseException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static String millisToFormat(Long millis) {
        return millisToFormat(millis.longValue(), DATE_FORMAT_SHOW);
    }

    public static String millisToFormat(String millis) {
        return millisToFormat(Long.parseLong(millis), DATE_FORMAT_SHOW);
    }

    public static String millisToTimeFormat(Long millis) {
        return millisToTimeFormat(millis.longValue(), TIME_FORMAT_SHOW);
    }

    public static String millisToTimeFormat(String millis) {
        return millisToTimeFormat(Long.parseLong(millis), TIME_FORMAT_SHOW);
    }

    public static String millisToFormat(String millis, String format) {
        return millisToFormat(Long.parseLong(millis), format);
    }

    public static String millisToFormat(long millis, String format) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeZone(tz);
        cal.setTimeInMillis(millis);
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.ENGLISH);
        sdf.setTimeZone(TimeZone.getDefault());
        return sdf.format(cal.getTime());
    }

    public static String millisToTimeFormat(long millis, String format) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeZone(tz);
        cal.setTimeInMillis(millis);
        SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.ENGLISH);
        sdf.setTimeZone(TimeZone.getDefault());
        return sdf.format(cal.getTime());
    }
}
