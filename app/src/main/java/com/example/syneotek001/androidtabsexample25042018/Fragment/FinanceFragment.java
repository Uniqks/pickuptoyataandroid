package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.example.syneotek001.androidtabsexample25042018.Adapter.LeaseSpinnerAdapter;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;

import java.util.ArrayList;

public class FinanceFragment extends Fragment implements OnBackPressed{


    Spinner spinnerCustom,spinnerRatePercentage;
    ArrayList<String> countries,arrTerms;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        View view = inflater.inflate(R.layout.fragment_finance, container, false);

        spinnerCustom = view.findViewById(R.id.spinnerCustom);
        spinnerRatePercentage = view.findViewById(R.id.spinnerRatePercentage);


        countries = new ArrayList<>();
        countries.add("Weekly");
        countries.add("Monthly");
        countries.add("Yearly");

        arrTerms = new ArrayList<>();
        arrTerms.add("21");
        arrTerms.add("24");
        arrTerms.add("27");
        arrTerms.add("30");
        arrTerms.add("36");
        arrTerms.add("39");


        initCustomSpinner(countries);
        initCustomSpinnerTerms(arrTerms);

        return view;
    }

    private void initCustomSpinner(ArrayList<String> countries) {

        LeaseSpinnerAdapter customSpinnerAdapter = new LeaseSpinnerAdapter(getActivity(), countries);
        spinnerCustom.setAdapter(customSpinnerAdapter);
        spinnerCustom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void initCustomSpinnerTerms(ArrayList<String> countries) {

        LeaseSpinnerAdapter customSpinnerAdapter = new LeaseSpinnerAdapter(getActivity(), countries);
        spinnerRatePercentage.setAdapter(customSpinnerAdapter);
        spinnerRatePercentage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    public void replaceFragment(Fragment fragment) {
        android.support.v4.app.FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        if (getActivity()!=null) {
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }
    @Override
    public void onResume() {
        super.onResume();

    }
}
