package com.example.syneotek001.androidtabsexample25042018.Utils;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.util.Base64;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;

/**
 * Created by Jatin on 9/13/2016.
 */
public class Utils {
    // Created A Static Retrofit Service Method For Getting reference to the retrofit service method


    public static final String PREF_NAME = "pref_name";
    public static final String PREF_USER_ID = "pref_user_id";
    Context context;
    static Utils utils;
    SharedPreferences pref;
    SharedPreferences.Editor edit;

    public static final boolean SHOULD_PRINT_LOG = true;

    public static String TEMP_1= "temp_text_1";
    public static String TEMP_POS= "temp_pos";

    public static String EXTRA_COLOR = "extra_color";
    public static String EXTRA_POS = "extra_pos";


    public static boolean checkData(String string) {
        return !string.isEmpty() && string.trim().length() != 0;
    }


    public static boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }
    public static byte[] getBytesFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        return stream.toByteArray();
    }

    public static String getStringFromBitmap(Bitmap bitmap){
        return Base64.encodeToString(getBytesFromBitmap(bitmap),
                Base64.NO_WRAP);
    }

    public static  Bitmap getBitmapFromBytes(byte[] bytes){
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }

    public Utils(Context context) {
        this.context = context;
        pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        edit = pref.edit();
    }





    public synchronized static Utils getInstance(Context context) {
        if (utils == null) {
            utils = new Utils(context);

        }
        return utils;
    }
    public void setInt(String key, int value) {
        edit.putInt(key, value);
        edit.commit();
    }

    public int getInt(String key) {
        return pref.getInt(key, 0);
    }
    public void setString(String key, String value) {
        edit.putString(key, value);
        edit.commit();
    }


    public static void Toast(Activity act, String msg)
    {
        Toast.makeText(act,msg,Toast.LENGTH_SHORT).show();
    }

    public void setBoolean(String key, boolean value) {
        edit.putBoolean(key, value);
        edit.commit();
    }

    public void clearPref() {

        edit.clear();
        edit.commit();
    }

    public static String toHex(int argb) {
        StringBuilder sb = new StringBuilder();
        //sb.append(toHexString((byte) Color.alpha(argb)));
        sb.append(toHexString((byte) Color.red(argb)));
        sb.append(toHexString((byte) Color.green(argb)));
        sb.append(toHexString((byte) Color.blue(argb)));
        return sb.toString();
    }

    private static String toHexString(byte v) {
        String hex = Integer.toHexString(v & 0xff);
        if (hex.length() == 1) {
            hex = "0" + hex; //$NON-NLS-1$
        }
        return hex.toUpperCase();
    }


}

