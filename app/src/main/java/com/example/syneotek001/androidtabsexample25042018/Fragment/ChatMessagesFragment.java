package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.syneotek001.androidtabsexample25042018.Adapter.ChatMessageItemAdapter;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.GlideUtils;
import com.example.syneotek001.androidtabsexample25042018.Utils.TimeStamp;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;
import com.example.syneotek001.androidtabsexample25042018.model.ChatMessageModel;
import com.example.syneotek001.androidtabsexample25042018.model.ChatModel;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;

public class ChatMessagesFragment extends Fragment implements OnClickListener, OnBackPressed {
    public static String BUNDLE_CHAT_FRIEND_MODEL = "chat_friend_data";
    private Animation animScaleIn;
    private Animation animScaleOut;
    ChatMessageItemAdapter mAdapter;
    ArrayList<ChatMessageModel> mChatMessageList = new ArrayList<>();
    ChatModel mChatModel;

    public EditText etMessage;
    public ImageView ivBack, ivFriendImage, ivOnlineIndicator, ivCamera, ivEmoticon, ivSendMessage, ivUploadFile;
    public LinearLayout llChatBox;
    public RecyclerView recyclerViewMessage;
    public RelativeLayout rel_NoMessageFound;
    public View viewDivider, viewDividerTop;
    TextView tvFriendName, tvLastSeen, tvNoMessageFound, tvFriendLetter;


    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mview = inflater.inflate(R.layout.activity_chat_message_list, container, false);

        etMessage = mview.findViewById(R.id.etMessage);
        ivCamera = mview.findViewById(R.id.ivCamera);
        ivEmoticon = mview.findViewById(R.id.ivEmoticon);
        ivSendMessage = mview.findViewById(R.id.ivSendMessage);
        ivUploadFile = mview.findViewById(R.id.ivUploadFile);
        llChatBox = mview.findViewById(R.id.llChatBox);
        recyclerViewMessage = mview.findViewById(R.id.recyclerViewMessage);
        viewDivider = mview.findViewById(R.id.viewDivider);
        rel_NoMessageFound = mview.findViewById(R.id.rel_NoMessageFound);
        ivBack = mview.findViewById(R.id.ivBack);
        ivFriendImage = mview.findViewById(R.id.ivFriendImage);
        tvFriendName = mview.findViewById(R.id.tvFriendName);
        tvLastSeen = mview.findViewById(R.id.tvLastSeen);
        ivOnlineIndicator = mview.findViewById(R.id.ivOnlineIndicator);
        tvNoMessageFound = mview.findViewById(R.id.tvNoMessageFound);
        tvFriendLetter = mview.findViewById(R.id.tvFriendLetter);
        ivBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        animScaleIn = AnimationUtils.loadAnimation(getContext(), R.anim.anim_scale_in);
        animScaleOut = AnimationUtils.loadAnimation(getActivity(), R.anim.anim_scale_out);
        setUpRecyclerView();
        setEditTexEvent();
        setClickEvents();
        setHasOptionsMenu(false);
        return mview;
    }

    @Override
    public void onBackPressed() {
        getActivity().getSupportFragmentManager().popBackStack();
    }

    class C05811 implements TextWatcher {

        class C05801 implements AnimationListener {
            C05801() {
            }

            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                ivSendMessage.setVisibility(View.VISIBLE);
            }

            public void onAnimationRepeat(Animation animation) {
            }
        }

        C05811() {
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (charSequence.length() == 1 && ivSendMessage.getVisibility() != View.VISIBLE) {
                ivSendMessage.startAnimation(animScaleOut);
                animScaleOut.setAnimationListener(new C05801());
            } else if (charSequence.length() == 0 && ivSendMessage.getVisibility() != View.GONE) {
                ivSendMessage.startAnimation(animScaleIn);
                ivSendMessage.setVisibility(View.GONE);
            }
        }

        public void afterTextChanged(Editable editable) {
        }
    }

    class C05822 implements OnClickListener {
        C05822() {
        }

        public void onClick(View view) {
            getActivity().onBackPressed();
        }
    }


    private void setClickEvents() {
        ivSendMessage.setOnClickListener(this);
        ivEmoticon.setOnClickListener(this);
        ivCamera.setOnClickListener(this);
        ivUploadFile.setOnClickListener(this);
    }

    private void setEditTexEvent() {
        etMessage.addTextChangedListener(new C05811());
    }

    private void setUpRecyclerView() {
        if (getArguments() != null) {
            this.mChatModel = (ChatModel) getArguments().getSerializable(BUNDLE_CHAT_FRIEND_MODEL);
            setupChatToolBarWithBackArrow(mChatModel, new GlideUtils(getActivity()));
            mChatMessageList.add(new ChatMessageModel(mChatModel.getName(), this.mChatModel.getImage(), "Hi", 1526119235, ChatMessageModel.Type.RECEIVED));
            mChatMessageList.add(0, new ChatMessageModel(mChatModel.getName(), mChatModel.getImage(), "Hi", 1526119235, ChatMessageModel.Type.RECEIVED));
            mChatMessageList.add(0, new ChatMessageModel("You", "", "Hello", 1526119235, ChatMessageModel.Type.SENT));
            mChatMessageList.add(0, new ChatMessageModel(mChatModel.getName(), mChatModel.getImage(), "How are you?", 1526119235, ChatMessageModel.Type.RECEIVED));
            mChatMessageList.add(0, new ChatMessageModel("You", "", "I am fine. \nWhat about you?", 1526119235, ChatMessageModel.Type.SENT));
            mChatMessageList.add(0, new ChatMessageModel(mChatModel.getName(), mChatModel.getImage(), "Good!", 1526119235, ChatMessageModel.Type.RECEIVED));
            mChatMessageList.add(0, new ChatMessageModel(mChatModel.getName(), mChatModel.getImage(), "Let's go for party this weekend. \nSince long time we didn't do any party. \nWhat you think?", 1526119235, ChatMessageModel.Type.RECEIVED));
            LinearLayoutManager lm = new LinearLayoutManager(getActivity());
            lm.setReverseLayout(true);
            recyclerViewMessage.setLayoutManager(lm);
            recyclerViewMessage.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_from_bottom));
            mAdapter = new ChatMessageItemAdapter(getActivity(), mChatMessageList);
            recyclerViewMessage.setAdapter(mAdapter);
            refreshData();
            return;
        }
        Toast.makeText(getActivity(), getResources().getString(R.string.something_went_wrong), Toast.LENGTH_SHORT).show();
        getActivity().onBackPressed();
    }

    private void refreshData() {
        if (mChatMessageList.size() > 0) {
            recyclerViewMessage.setVisibility(View.VISIBLE);
            rel_NoMessageFound.setVisibility(View.GONE);
            return;
        }
        recyclerViewMessage.setVisibility(View.GONE);
        rel_NoMessageFound.setVisibility(View.VISIBLE);
        tvNoMessageFound.setText(getResources().getString(R.string.no_message_found));
        tvNoMessageFound.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_vector_no_chats, 0, 0);
    }

    public void setupChatToolBarWithBackArrow(ChatModel chatModel, GlideUtils glideUtils) {
        /*title = tvFriendName;
        TextView tvLastSeen =tvLastSeen;
        TextView tvFriendLetter = tvFriendLetter;
        ImageView ivBack = ivBack;
        ImageView ivFriendImage = ivFriendImage;
        ImageView ivOnlineIndicator = ivOnlineIndicator;*/
        tvFriendName.setText(chatModel.getName() != null ? chatModel.getName() : "");
        tvLastSeen.setText(chatModel.isOnline() ? getResources().getString(R.string.online) : getResources().getString(R.string.last_seen_, new Object[]{TimeStamp.millisToTimeFormat(Long.valueOf(chatModel.getLastActive() * 1000))}));
        ivOnlineIndicator.setVisibility(chatModel.isOnline() ? View.VISIBLE : View.GONE);
        if (chatModel.getImage() == null || chatModel.getImage().length() <= 0) {
            String title = chatModel.getName().toUpperCase();
            if (title.length() > 0) {
                tvFriendLetter.setText(title.substring(0, 1).toUpperCase());
            } else {
                tvFriendLetter.setText("T");
            }
            glideUtils.loadImageCircular("", ivFriendImage, R.drawable.shape_circle_filled_white);
            tvFriendLetter.setVisibility(View.VISIBLE);
        } else {
            glideUtils.loadImageCircular(chatModel.getImage(), ivFriendImage);
            tvFriendLetter.setVisibility(View.GONE);
        }
        ivBack.setOnClickListener(new C05822());
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivSendMessage:
                if (etMessage.length() > 0) {
                    mChatMessageList.add(0, new ChatMessageModel("You", "", etMessage.getText().toString(), new Date().getTime() / 1000, ChatMessageModel.Type.SENT));
                    mAdapter.notifyDataSetChanged();
                    etMessage.setText("");
                    if (mChatMessageList.size() == 1) {
                        refreshData();
                        return;
                    }
                    return;
                }
                return;
            default:
                return;
        }
    }
}
