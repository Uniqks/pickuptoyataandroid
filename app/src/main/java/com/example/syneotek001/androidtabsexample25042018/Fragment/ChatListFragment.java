package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.TextView;


import com.example.syneotek001.androidtabsexample25042018.Adapter.ChatItemAdapter;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.model.ChatModel;

import java.util.ArrayList;

public class ChatListFragment extends Fragment {
    public static String BUNDLE_IS_DISPLAY_CONTACT = "is_display_contact";
    private boolean isDisplayContacts;
    ChatItemAdapter mAdapter;
    ArrayList<ChatModel> mCarList = new ArrayList<>();
    View view;

    public RecyclerView recyclerView;
    public TextView tvNoDataFound;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mview = inflater.inflate(R.layout.fragment_common_recycler_view_list, container, false);
        recyclerView = mview.findViewById(R.id.recyclerView);
        tvNoDataFound = mview.findViewById(R.id.tvNoDataFound);
        setUpRecyclerView();
        setHasOptionsMenu(false);
        return mview;
    }

    private void setUpRecyclerView() {
        if (getArguments() != null) {
            this.isDisplayContacts = getArguments().getBoolean(BUNDLE_IS_DISPLAY_CONTACT);
        }
        if (this.isDisplayContacts) {
            this.mCarList.add(new ChatModel("1", "Abc Patel", "https://pickeringtoyota.com/toyotasalesadmin/salesmanimages/1513327020_Koala.jpg", 0, true, 1526119235));
            this.mCarList.add(new ChatModel("2", "New User", "", 0, true, 1526119235));
            this.mCarList.add(new ChatModel("3", "Paresh Shah", "https://pickeringtoyota.com/toyotasalesadmin/salesmanimages/1523879002_image1.jpg", 0, false, 1526119235));
            this.mCarList.add(new ChatModel("4", "Test User", "", 0, false, 1526119235));
            this.mCarList.add(new ChatModel("5", "Yuvraj Sing", "", 0, true, 1526119235));
        } else {
            this.mCarList.add(new ChatModel("1", "Abc Patel", "https://pickeringtoyota.com/toyotasalesadmin/salesmanimages/1513327020_Koala.jpg", 2, true, 1526119235));
            this.mCarList.add(new ChatModel("2", "Test User", "", 1, false, 1526119235));
            this.mCarList.add(new ChatModel("3", "Paresh Shah", "https://pickeringtoyota.com/toyotasalesadmin/salesmanimages/1523879002_image1.jpg", 0, false, 1526119235));
        }
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_from_bottom));
        mAdapter = new ChatItemAdapter(getActivity(), this.mCarList);
        recyclerView.setAdapter(this.mAdapter);
        refreshData();
    }

    private void refreshData() {
        if (this.mCarList.size() > 0) {
           recyclerView.setVisibility(View.VISIBLE);
           tvNoDataFound.setVisibility(View.GONE);
            return;
        }
        recyclerView.setVisibility(View.GONE);
       tvNoDataFound.setVisibility(View.VISIBLE);
        if (this.isDisplayContacts) {
            tvNoDataFound.setText(getResources().getString(R.string.no_contact_found));
            tvNoDataFound.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_vector_no_contact, 0, 0);
            return;
        }
       tvNoDataFound.setText(getResources().getString(R.string.no_chat_found));
       tvNoDataFound.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_vector_no_chats, 0, 0);
    }
}
