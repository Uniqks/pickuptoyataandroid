package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.TimeStamp;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnLeadItemViewClickListener;
import com.example.syneotek001.androidtabsexample25042018.model.LeadItemModel;

import java.util.ArrayList;

public class LeadSoldInactiveItemAdapter extends RecyclerView.Adapter<LeadSoldInactiveItemAdapter.MyViewHolder> implements OnClickListener  {
    private Context mContext;
    private ArrayList<LeadItemModel> mData = new ArrayList();
    private OnLeadItemViewClickListener onItemClickListener;
    private int lastSelectedPosition = 1;


    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivLeadLabel:
                if (LeadSoldInactiveItemAdapter.this.onItemClickListener != null) {
                    LeadSoldInactiveItemAdapter.this.onItemClickListener.onLeadLabelClicked(lastSelectedPosition);
                    return;
                }
                return;
            default:
                return;
        }
    }

    class MyViewHolder extends ViewHolder {
         ImageView ivLeadLabel;
         TextView tvAssignDate,tvCustomerId,tvName;

        MyViewHolder(View view) {
            super(view);
            ivLeadLabel=view.findViewById(R.id.ivLeadLabel);
            tvAssignDate=view.findViewById(R.id.tvAssignDate);
            tvCustomerId=view.findViewById(R.id.tvCustomerId);
            tvName=view.findViewById(R.id.tvName);
        }
    }

    public LeadSoldInactiveItemAdapter(Context context, ArrayList<LeadItemModel> list, OnLeadItemViewClickListener onItemClickListener) {
        this.mContext = context;
        this.mData = list;
        this.onItemClickListener = onItemClickListener;
    }

    public int getItemViewType(int position) {
        return 1;
    }

    /*public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder((ItemLeadSoldInactiveBinding) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_lead_sold_inactive, parent, false));
    }*/

    @NonNull
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_lead_sold_inactive, parent, false);

        return new MyViewHolder(v);
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        LeadItemModel mLeadItem = mData.get(position);
        holder.tvCustomerId.setText(mLeadItem.getCustomerId());
        holder.tvAssignDate.setText(TimeStamp.millisToTimeFormat(mLeadItem.getAssignDate() * 1000));
        holder.tvName.setText(mLeadItem.getClientName());

         holder.ivLeadLabel.setOnClickListener(this);


        holder.itemView.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (onItemClickListener != null) {
                    onItemClickListener.onLeadDetailIconClicked(position);
                }
            }
        });

        String leadStatus = mLeadItem.getLeadStatus();
        int obj = -1;
        switch (leadStatus.hashCode()) {
            case 49:
                if (leadStatus.equals("1")) {
                    obj = 0;
                    break;
                }
                break;
            case 50:
                if (leadStatus.equals("2")) {
                    obj = 1;
                    break;
                }
                break;
            case 51:
                if (leadStatus.equals("3")) {
                    obj = 2;
                    break;
                }
                break;
            case 52:
                if (leadStatus.equals("4")) {
                    obj = 3;
                    break;
                }
                break;
            case 53:
                if (leadStatus.equals("5")) {
                    obj = 4;
                    break;
                }
                break;
            case 54:
                if (leadStatus.equals("6")) {
                    obj = 5;
                    break;
                }
                break;
        }
            switch (obj) {
                case 0:
                    holder.ivLeadLabel.setImageResource(R.drawable.ic_rounded_corner_filter_hot);
                    return;
                case 1:
                    holder.ivLeadLabel.setImageResource(R.drawable.ic_rounded_corner_lead_contact);
                    return;
                case 2:
                    holder.ivLeadLabel.setImageResource(R.drawable.ic_rounded_corner_lead_under_contract);
                    return;
                case 3:
                    holder.ivLeadLabel.setImageResource(R.drawable.ic_rounded_corner_lead_closing);
                    return;
                case 4:
                    holder.ivLeadLabel.setImageResource(R.drawable.ic_rounded_corner_filter_closed);
                    return;
                case 5:
                    holder.ivLeadLabel.setImageResource(R.drawable.ic_rounded_corner_filter_dead);
                    return;
                default:
                    return;
            }

    }
    
    public int getItemCount() {
        return mData.size();
    }

    public void setLastSelectedPosition(int position) {
        this.lastSelectedPosition = position;
    }
}
