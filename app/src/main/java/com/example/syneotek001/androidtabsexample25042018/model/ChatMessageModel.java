package com.example.syneotek001.androidtabsexample25042018.model;

import android.text.format.DateFormat;

import java.lang.reflect.Type;
import java.util.concurrent.TimeUnit;

public class ChatMessageModel {
    private String image;
    private String message;
    private String sender;
    private long timestamp;
    private Type type;


    public enum Type {
        SENT,
        RECEIVED
    }

    public ChatMessageModel(String message, long timestamp, Type type) {
        this.sender = "";
        this.image = "";
        this.message = "";
        this.message = message;
        this.timestamp = timestamp;
        this.type = type;
    }

    public ChatMessageModel(String sender, String image, String message, long timestamp, Type type) {
        this(message, timestamp, type);
        this.sender = sender;
        this.image = image;
    }

    public long getTimestamp() {
        return this.timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Type getType() {
        return this.type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getFormattedTime() {
        if (System.currentTimeMillis() - this.timestamp < TimeUnit.DAYS.toMillis(1)) {
            return DateFormat.format("hh:mm a", this.timestamp * 1000).toString();
        }
        return DateFormat.format("dd/MM/yyyy - hh:mm a", this.timestamp * 1000).toString();
    }

    public String getSender() {
        return this.sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getImage() {
        return this.image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String toString() {
        return "ChatMessageModel{sender='" + this.sender + '\'' + ", message='" + this.message + '\'' + ", image='" + this.image + '\'' + ", timestamp=" + this.timestamp + ", type=" + this.type + '}';
    }
}
