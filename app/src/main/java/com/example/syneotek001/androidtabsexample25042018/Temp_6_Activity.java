/**
 * Copyright 2014 Magnus Woxblom
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.syneotek001.androidtabsexample25042018;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.SeekBar;

import com.example.syneotek001.androidtabsexample25042018.Utils.LogUtils;
import com.example.syneotek001.androidtabsexample25042018.Utils.Utils;
import com.example.syneotek001.androidtabsexample25042018.databinding.ActivityTemp6Binding;


public class Temp_6_Activity extends AppCompatActivity implements  View.OnClickListener{


    ActivityTemp6Binding binding;
    Bundle extra;String pos="";
    String mAlignment ="center"; int mColor =Color.BLACK;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding =  DataBindingUtil.setContentView(Temp_6_Activity.this,R.layout.activity_temp_6);

        extra = getIntent().getExtras();

        if(extra != null)
        {
            pos = extra.getString(Utils.TEMP_POS);
        }

        binding.ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        String[] mlist = getResources().getStringArray(R.array.spin_family);

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_list, R.id.spin_item, mlist);
        binding.spinFamily.setAdapter(spinnerArrayAdapter);

        String[] mlist1 = getResources().getStringArray(R.array.spin_weight);

        ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>(this, R.layout.spinner_list, R.id.spin_item, mlist1);
        binding.spinWeight.setAdapter(spinnerArrayAdapter1);

        binding.btnCenter.setOnClickListener(this);
        binding.btnRight.setOnClickListener(this);
        binding.btnLeft.setOnClickListener(this);
        mAlignment ="center";
        binding.btnCenter.setBackgroundColor(getResources().getColor(R.color.blue));
        binding.btnLeft.setBackgroundColor(Color.TRANSPARENT);
        binding.btnRight.setBackgroundColor(Color.TRANSPARENT);
        binding.btnCenter.setTextColor(getResources().getColor(R.color.white));
        binding.btnLeft.setTextColor(getResources().getColor(R.color.black));
        binding.btnRight.setTextColor(getResources().getColor(R.color.black));


        binding.etBorderColor.setText(getString(R.string.black));
        binding.etBtnBgColor.setText(getString(R.string.black));
        binding.etTxtColor.setText(getString(R.string.black));
        binding.etBgColor.setText(getString(R.string.black));


        binding.btnBgColor.setBackgroundColor(Color.BLACK);
        binding.btnTxtColor.setBackgroundColor(Color.BLACK);
        binding.btnBorderColor.setBackgroundColor(Color.BLACK);
        binding.btnColorBg.setBackgroundColor(Color.BLACK);


        binding.etBgColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(Temp_6_Activity.this, ColorAlertActivity.class).putExtra(Utils.EXTRA_POS, 1), 1);
            }
        });

        binding.etTxtColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(Temp_6_Activity.this, ColorAlertActivity.class).putExtra(Utils.EXTRA_POS, 2), 2);
            }
        });
        binding.etBtnBgColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(Temp_6_Activity.this, ColorAlertActivity.class).putExtra(Utils.EXTRA_POS, 3), 3);
            }
        });

        binding.etBorderColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(Temp_6_Activity.this, ColorAlertActivity.class).putExtra(Utils.EXTRA_POS, 4), 4);
            }
        });



        binding.seekBorder.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                binding.txtBorder.setText(i+"px");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        binding.seekRadius.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                binding.txtRadius.setText(i+"px");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        binding.seekFontSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                binding.txtFont.setText(i+"px");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        binding.btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String mTop = binding.etPtop.getText().toString();
                String mRight = binding.etPright.getText().toString();
                String mBottom = binding.etPbottom.getText().toString();
                String mLeft = binding.etPleft.getText().toString();

                mTop = mTop.equals("") ?"0px":mTop+"px";
                mRight = mRight.equals("") ?"0px":mRight+"px";
                mBottom = mBottom.equals("") ?"0px":mBottom+"px";
                mLeft = mLeft.equals("") ?"0px":mLeft+"px";

                String mPadding =mTop+" "+mRight+ " "+mBottom+" "+mLeft;

                String MTop = binding.etMtop.getText().toString();
                String MRight = binding.etMright.getText().toString();
                String MBottom = binding.etMbottom.getText().toString();
                String MLeft = binding.etMleft.getText().toString();

                MTop = MTop.equals("") ?"0px":MTop+"px";
                MRight = MRight.equals("") ?"0px":MRight+"px";
                MBottom = MBottom.equals("") ?"0px":MBottom+"px";
                MLeft = MLeft.equals("") ?"0px":MLeft+"px";

                String mMargin =MTop+" "+MRight+ " "+MBottom+" "+MLeft;

                //String mBg_color = hex2Rgb(binding.etBgColor.getText().toString());
                String mTxt_color = (binding.etTxtColor.getText().toString());
                String mBtn_bg_color = (binding.etBtnBgColor.getText().toString());
                String mBorder_color = (binding.etBorderColor.getText().toString());


                String weight = binding.spinWeight.getSelectedItem().toString();
                String family = binding.spinFamily.getSelectedItem().toString();

                String mTitle = binding.etBtnText.getText().toString();
                String mUrl = binding.etUrl.getText().toString();


                String full_width = (binding.chBox.isChecked())? "block":"inline-block";
                String mFontSize = binding.seekFontSize.getProgress()+"px";
                String mBorderSize = binding.seekBorder.getProgress()+"px";
                String mRadiusSize = binding.seekRadius.getProgress()+"px";
                String mStyle = binding.spinStyle.getSelectedItem().toString();



                String mData ="<body bgcolor=\"" + binding.etBgColor.getText().toString() + "\"><table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" align=\""+mAlignment+"\" width=\"100%\" class=\"button\" style=\"text-align: right;\">" +
                        "<tbody><tr><td class=\"button\" style=\"padding: "+mMargin+";\"><a href=\"#"+mUrl+"\" data-default=\"1\" " +
                        "class=\"button-1\" style=\"background-color: "+mTxt_color+"; font-family: "+family+"; font-size: "+mFontSize+"; line-height: 19px; " +
                        "display: "+full_width+"; border-radius: "+mRadiusSize+"; border: "+mBorderSize+" "+mStyle+" "+mBorder_color+"; text-align: center; text-decoration: none; " +
                        "font-weight: "+weight+"; margin: 0px; width: auto; padding: "+mPadding+"; background-color: "+mBtn_bg_color+";\">"+mTitle+"</a></td></tr></tbody></table></</body>";


                LogUtils.i(" mData "+mData);

                Intent i = getIntent();
                i.putExtra(Utils.TEMP_1,mData);
                i.putExtra(Utils.TEMP_POS,pos);
                setResult(0,i);
                finish();
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            mColor = data.getIntExtra(Utils.EXTRA_COLOR, Color.BLACK);
            if (requestCode == 1) {
                binding.etBgColor.setText("#" + Utils.toHex(mColor));
                binding.btnBgColor.setBackgroundColor(Color.parseColor("#" + Utils.toHex(mColor)));
            } else  if (requestCode == 2) {
                binding.etTxtColor.setText("#" + Utils.toHex(mColor));
                binding.btnTxtColor.setBackgroundColor(Color.parseColor("#" + Utils.toHex(mColor)));
            }else  if (requestCode == 3) {
                binding.etBtnBgColor.setText("#" + Utils.toHex(mColor));
                binding.btnColorBg.setBackgroundColor(Color.parseColor("#" + Utils.toHex(mColor)));
            } else  if (requestCode == 4) {
                binding.etBorderColor.setText("#" + Utils.toHex(mColor));
                binding.btnBorderColor.setBackgroundColor(Color.parseColor("#" + Utils.toHex(mColor)));
            }

        }

    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_center:
                mAlignment = "center";
                binding.btnCenter.setBackgroundColor(getResources().getColor(R.color.blue));
                binding.btnLeft.setBackgroundColor(Color.TRANSPARENT);
                binding.btnRight.setBackgroundColor(Color.TRANSPARENT);
                binding.btnCenter.setTextColor(getResources().getColor(R.color.white));
                binding.btnLeft.setTextColor(getResources().getColor(R.color.black));
                binding.btnRight.setTextColor(getResources().getColor(R.color.black));
                break;
            case R.id.btn_left:
                mAlignment = "left";
                binding.btnCenter.setBackgroundColor(Color.TRANSPARENT);
                binding.btnLeft.setBackgroundColor(getResources().getColor(R.color.blue));
                binding.btnRight.setBackgroundColor(Color.TRANSPARENT);
                binding.btnCenter.setTextColor(getResources().getColor(R.color.black));
                binding.btnLeft.setTextColor(getResources().getColor(R.color.white));
                binding.btnRight.setTextColor(getResources().getColor(R.color.black));
                break;
            case R.id.btn_right:
                mAlignment = "right";
                binding.btnCenter.setBackgroundColor(Color.TRANSPARENT);
                binding.btnLeft.setBackgroundColor(Color.TRANSPARENT);
                binding.btnRight.setBackgroundColor(getResources().getColor(R.color.blue));
                binding.btnCenter.setTextColor(getResources().getColor(R.color.black));
                binding.btnLeft.setTextColor(getResources().getColor(R.color.black));
                binding.btnRight.setTextColor(getResources().getColor(R.color.white));
                break;
        }

    }
}
