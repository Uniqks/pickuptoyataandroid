package com.example.syneotek001.androidtabsexample25042018.Utils;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestOptions;
import com.example.syneotek001.androidtabsexample25042018.R;

public class GlideUtils {
    private Context context;

    public GlideUtils(Context context) {
        this.context = context;
    }

    public void loadImageSimple(String url, ImageView view) {
        Glide.with(this.context).load(url).apply(new RequestOptions().placeholder(R.drawable.shape_circle_filled_gray).error(R.drawable.shape_circle_filled_gray).diskCacheStrategy(DiskCacheStrategy.RESOURCE)).transition(DrawableTransitionOptions.withCrossFade()).into(view);
    }

    public void loadImageCircular(String url, ImageView view) {
        loadImageCircular(url, view, R.drawable.shape_circle_filled_gray);
    }

    public void loadImageCircular(String url, ImageView view, int drawable) {
        Glide.with(context).load(url).apply(new RequestOptions().centerCrop().circleCrop().placeholder(drawable).error(drawable).diskCacheStrategy(DiskCacheStrategy.RESOURCE)).transition(DrawableTransitionOptions.withCrossFade()).into(view);
    }
}
