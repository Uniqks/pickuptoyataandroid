package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import com.example.syneotek001.androidtabsexample25042018.R;

import java.util.ArrayList;
import java.util.List;


public class EditTaskFragment extends Fragment {
ImageView iv_Close;
Spinner category_spinner,status_spinner;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_edit_task, container, false);
        iv_Close=view.findViewById(R.id.iv_Close);
        iv_Close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });
        category_spinner=view.findViewById(R.id.category_spinner);
        status_spinner=view.findViewById(R.id.status_spinner);

        List<String> category = new ArrayList<String>();
        category.add("Waiting For Reply");
        category.add("Waiting For Co_Singer");
        category.add("Waiting For Final Signature");
        category.add("Waiting For Pick Up");
        category.add("Waiting For Coming Up");
        category.add("Waiting For Finalize");
        category.add("making Decision");
        category.add("Not Intersted");
        category.add("Must Follow-up");
        category.add("Coming To  Test Drive");
        category.add("Coming To See  Me");
        category.add("Asking To Exceptional");
        category.add("Getting More Quotes");
        category.add("Waiting");

        ArrayAdapter<String> CategoryAdpter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, category);
        CategoryAdpter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        category_spinner.setAdapter(CategoryAdpter);

        List<String> status = new ArrayList<String>();
        status.add("Follow-up");
        status.add("Pending");
        status.add("Not Reply");
        status.add("Completed");

        ArrayAdapter<String> statusAdpter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, status);
        statusAdpter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        status_spinner.setAdapter(statusAdpter);
        return view;
    }
}
