package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.GlideUtils;
import com.example.syneotek001.androidtabsexample25042018.model.ChatMessageModel;

import java.util.ArrayList;


public class ChatMessageItemAdapter extends Adapter {
    private final int VIEW_TYPE_RECEIVED = 0;
    private final int VIEW_TYPE_SENT = 1;
    private GlideUtils glideUtils;
    private Context mContext;
    ArrayList<ChatMessageModel> mData;

    private class ReceivedViewHolder extends ViewHolder {
        TextView tvFriendName, tvMessage, tvDateTime, tvFriendLetter;
        ImageView ivFriendImage;

        ReceivedViewHolder(View view) {
            super(view);
            tvFriendName = view.findViewById(R.id.tvFriendName);
            tvMessage = view.findViewById(R.id.tvMessage);
            tvDateTime = view.findViewById(R.id.tvDateTime);
            tvFriendLetter = view.findViewById(R.id.tvFriendLetter);
            ivFriendImage = view.findViewById(R.id.ivFriendImage);
//            this.mBinder = mBinder;
        }
    }

    private class SentViewHolder extends ViewHolder {
        TextView tvMyName, tvMessage, tvDateTime, tvMyLetter;
        ImageView ivMyImage;

        SentViewHolder(View view) {
            super(view);
            tvMyName = view.findViewById(R.id.tvMyName);
            tvMessage = view.findViewById(R.id.tvMessage);
            tvDateTime = view.findViewById(R.id.tvDateTime);
            tvMyLetter = view.findViewById(R.id.tvMyLetter);
            ivMyImage = view.findViewById(R.id.ivMyImage);
//            this.mBinder = mBinder;
        }
    }

    public ChatMessageItemAdapter(Context mContext, ArrayList<ChatMessageModel> mData) {
        this.mContext = mContext;
        this.mData = mData;
        this.glideUtils = new GlideUtils(mContext);
    }

    @NonNull
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 0:
                View rview = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_message_received, parent, false);
                return new ReceivedViewHolder(rview);
            default:
                View sview = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_message_sent, parent, false);
                return new SentViewHolder(sview);
        }
    }

    public void onBindViewHolder(ViewHolder holder, int position) {
        ChatMessageModel mMessage = (ChatMessageModel) this.mData.get(position);
        String title;
        switch (holder.getItemViewType()) {
            case 0:
                ReceivedViewHolder receivedHolder = (ReceivedViewHolder) holder;
                receivedHolder.tvFriendName.setText(mMessage.getSender());
                receivedHolder.tvMessage.setText(mMessage.getMessage());
                receivedHolder.tvDateTime.setText(mMessage.getFormattedTime());
                if (mMessage.getImage() == null || mMessage.getImage().length() <= 0) {
                    title = mMessage.getSender().toUpperCase();
                    if (title.length() > 0) {
                        receivedHolder.tvFriendLetter.setText(title.substring(0, 1).toUpperCase());
                    } else {
                        receivedHolder.tvFriendLetter.setText("T");
                    }
                    receivedHolder.tvFriendLetter.setVisibility(View.VISIBLE);
                    glideUtils.loadImageCircular("", receivedHolder.ivFriendImage, R.drawable.shape_circle_filled_light_gray);
                    return;
                }
                this.glideUtils.loadImageCircular(mMessage.getImage(), receivedHolder.ivFriendImage, R.drawable.shape_circle_filled_light_gray);
                receivedHolder.tvFriendLetter.setVisibility(View.GONE);
                return;
            default:
                SentViewHolder sentHolder = (SentViewHolder) holder;
                sentHolder.tvMyName.setText(mMessage.getSender());
                sentHolder.tvMessage.setText(mMessage.getMessage());
                sentHolder.tvDateTime.setText(mMessage.getFormattedTime());
                if (mMessage.getImage() == null || mMessage.getImage().length() <= 0) {
                    title = mMessage.getSender().toUpperCase();
                    if (title.length() > 0) {
                        sentHolder.tvMyLetter.setText(title.substring(0, 1).toUpperCase());
                    } else {
                        sentHolder.tvMyLetter.setText("Y");
                    }
                    sentHolder.tvMyLetter.setVisibility(View.VISIBLE);
                    this.glideUtils.loadImageCircular("", sentHolder.ivMyImage, R.drawable.shape_circle_filled_light_gray);
                    return;
                }
                this.glideUtils.loadImageCircular(mMessage.getImage(), sentHolder.ivMyImage, R.drawable.shape_circle_filled_light_gray);
                sentHolder.tvMyLetter.setVisibility(View.GONE);
                return;
        }
    }

    public int getItemCount() {
        return this.mData.size();
    }

    public int getItemViewType(int position) {
        return ((ChatMessageModel) this.mData.get(position)).getType().equals(ChatMessageModel.Type.RECEIVED) ? 0 : 1;
    }
}
