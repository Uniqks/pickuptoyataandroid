package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatDelegate;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Adapter.FragmentTabsPagerAdapter;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnBackPressed;
import com.example.syneotek001.androidtabsexample25042018.model.AnnouncementItemModel;

import java.util.ArrayList;

public class HelpCenterFragment extends Fragment implements OnBackPressed{
    public static final String BUNDLE_UPDATE_LABEL_POSITION = "position";
    public static final String BUNDLE_UPDATE_LABEL_VALUE = "labelValue";
    public static final int LEAD_LIST_UPDATE_LABEL = 1;
    ArrayList<AnnouncementItemModel> leadItemArray = new ArrayList<>();
    View view;

    TabLayout tab_layout;
    ViewPager viewPager;
    FragmentTabsPagerAdapter mAdapter;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        View view = inflater.inflate(R.layout.fragment_helpcenter, container, false);
        ImageView ivBack = view.findViewById(R.id.iv_back);
        ImageView ivAdd = view.findViewById(R.id.ivAdd);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity()!=null) {
                    getActivity().getSupportFragmentManager().popBackStack();
                }
            }
        });

        ivAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                replaceFragment(new AddTicketFragment());
            }
        });

        tab_layout = view.findViewById(R.id.tab_layout);
        viewPager = view.findViewById(R.id.viewPager);
        mAdapter = new FragmentTabsPagerAdapter(getChildFragmentManager());
        mAdapter.addFragment(new HelpcenterAdminFragment(),getString(R.string.str_from_admin));
        mAdapter.addFragment(new HelpcenterMymessageFragment(),getString(R.string.str_my_messages));

        viewPager.setAdapter(mAdapter);
        viewPager.setOffscreenPageLimit(2);
        tab_layout.setupWithViewPager(viewPager);
        tab_layout.setTabMode(1);

        return view;
    }

    public void replaceFragment(Fragment fragment) {
        android.support.v4.app.FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        if (getActivity()!=null) {
            getActivity().getSupportFragmentManager().popBackStack();
        }
    }
    @Override
    public void onResume() {
        super.onResume();

        mAdapter = new FragmentTabsPagerAdapter(getChildFragmentManager());
        mAdapter.addFragment(new HelpcenterAdminFragment(),getString(R.string.str_from_admin));
        mAdapter.addFragment(new HelpcenterMymessageFragment(),getString(R.string.str_my_messages));

        viewPager.setAdapter(mAdapter);
        viewPager.setOffscreenPageLimit(2);
        tab_layout.setupWithViewPager(viewPager);
        tab_layout.setTabMode(1);

    }
}
