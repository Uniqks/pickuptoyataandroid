package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Fragment.NoteDetailFragment;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.model.LeadNoteModel;

import java.util.ArrayList;
import java.util.List;

public class LeadWebActivityItemAdapter extends Adapter<LeadWebActivityItemAdapter.MyViewHolder> {
    private final Animation anim;
    private Context mContext;
    private List<LeadNoteModel> mData = new ArrayList();

    class MyViewHolder extends ViewHolder {
       ImageView ivLeadLabel,ivNoteAttachment;
        LinearLayout llNote;
         TextView tvDescription,tvTitle;

        MyViewHolder(View view) {
            super(view);
            ivLeadLabel=view.findViewById(R.id.ivLeadLabel);
            ivNoteAttachment=view.findViewById(R.id.ivNoteAttachment);
            llNote=view.findViewById(R.id.llNote);
            tvDescription=view.findViewById(R.id.tvDescription);
            tvTitle=view.findViewById(R.id.tvTitle);




        }
    }

    public LeadWebActivityItemAdapter(Context context, ArrayList<LeadNoteModel> list) {
        this.mContext = context;
        this.mData = list;
        this.anim = AnimationUtils.loadAnimation(mContext, R.anim.anim_scale_in_out);
    }
    public int getItemViewType(int position) {


        return 1;
    }


    @NonNull
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_webactivity, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        LeadNoteModel mLeadNoteModel = (LeadNoteModel) this.mData.get(position);
        holder.tvTitle.setText(mLeadNoteModel.getTitle());
        holder.tvDescription.setText(mLeadNoteModel.getDescription());
        holder.itemView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ((HomeActivity) mContext).replaceFragment( new NoteDetailFragment());
            }
        });
      /*  int pos=holder.getAdapterPosition();
        switch (pos)
        {
            case 0:
                ((HomeActivity) LeadNoteItemAdapter.this.mContext).pushFragment(((HomeActivity) LeadNoteItemAdapter.this.mContext).mCurrentTab, new NoteDetailFragment(), true, true);
                break;
                default:
                    break;

        }
*/



    }

    public int getItemCount() {
        return this.mData.size();
    }
}
