package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Adapter.TaskItemAdapter;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnLeadItemViewClickListener;
import com.example.syneotek001.androidtabsexample25042018.model.TaskItemModel;

import java.util.ArrayList;
import java.util.Calendar;

public class TaskListFragment extends Fragment implements OnLeadItemViewClickListener {
    public static final String BUNDLE_UPDATE_LABEL_POSITION = "position";
    public static final String BUNDLE_UPDATE_LABEL_VALUE = "labelValue";
    public static final int LEAD_LIST_UPDATE_LABEL = 1;
    ArrayList<TaskItemModel> leadItemArray = new ArrayList<>();
    TaskItemAdapter mAdapter;
    View view;
    public RecyclerView recyclerView;
    public TextView tvNoDataFound;
    String strTitle;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        View view = inflater.inflate(R.layout.fragment_task_list, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        tvNoDataFound = view.findViewById(R.id.tvNoDataFound);

        ImageView ivBack = view.findViewById(R.id.ivBack);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().popBackStack();
            }
        });

        ImageView ivAdd = view.findViewById(R.id.ivAdd);
        ivAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // open calendar page here
                ((HomeActivity) getActivity()).replaceFragment(new ViewTaskFragment());
            }
        });


        TextView tvTitle = view.findViewById(R.id.tvTitle);
        if (getArguments().getString("task_manager_title") != null && !getArguments().getString("task_manager_title").equals(""))
            strTitle = getArguments().getString("task_manager_title");
        else
            strTitle = getString(R.string.str_waiting_for_reply);
        if (strTitle != null && !strTitle.equals(""))
            tvTitle.setText(strTitle);

        setUpRecyclerView();
        return view;
    }

    private void setUpRecyclerView() {
        leadItemArray.clear();
        leadItemArray.add(new TaskItemModel("1", "Task Title 1", strTitle, "1", "2018-05-21 15:32:21", "Test Notes here"));
        leadItemArray.add(new TaskItemModel("2", "Task Title 2", strTitle, "2", "2018-04-10 18:32:21", "Test Notes here"));
        leadItemArray.add(new TaskItemModel("3", "Task Title 3", strTitle, "3", "2018-03-17 05:32:21", "Test Notes here"));
        leadItemArray.add(new TaskItemModel("4", "Task Title 4", strTitle, "4", "2018-05-21 15:32:21", "Test Notes here"));
        leadItemArray.add(new TaskItemModel("5", "Task Title 5", strTitle, "1", "2018-05-21 15:32:21", "Test Notes here"));
        leadItemArray.add(new TaskItemModel("6", "Task Title 6", strTitle, "2", "2018-05-21 15:32:21", "Test Notes here"));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.layout_animation_from_right));
        mAdapter = new TaskItemAdapter(getActivity(), leadItemArray, this);
        recyclerView.setAdapter(mAdapter);
    }

    public void onItemClicked(int position) {
    }

    public void onLeadLabelClicked(int position) {
        /*UpdateLeadLabelDialog dialog = new UpdateLeadLabelDialog();
        Bundle args = new Bundle();
        args.putString(BUNDLE_UPDATE_LABEL_VALUE, ((TaskItemModel) leadItemArray.get(position)).getLeadLabel());
        args.putInt("position", position);
        dialog.setArguments(args);
        dialog.setTargetFragment(this, 1);
        dialog.show(getFragmentManager().beginTransaction(), UpdateLeadLabelDialog.class.getSimpleName());*/
    }

    public void onLeadDetailIconClicked(int position) {

//        ((HomeActivity) getActivity()).replaceFragment(new LeadDetailFragment());

    }

    public void onLeadEmailIconClicked(int position) {
//        ((HomeActivity) getActivity()).replaceFragment(new ComposeFragment());
    }

    public void onLeadScheduleIconClicked(int position) {
//        ((HomeActivity) getActivity()).replaceFragment(new CalinderViewFragment());

        int year;
        int month;
        int day;
        final Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
//                                et_dob.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
//                                dob = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                        ((HomeActivity) getActivity()).replaceFragment(new AddSheduleFragment());
                    }
                }, year, month, day);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
        datePickerDialog.show();

    }

    public void onLeadInactiveIconClicked(int position) {
    }

    public void onLeadDealRequestClicked(int position) {
    }

    public void onLeadBusinessRequestClicked(int position) {
    }

    /*public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 1:
                if (resultCode == -1) {
                    Bundle bundle = data.getExtras();
                    if (bundle != null) {
                        String newLabel = bundle.getString(BUNDLE_UPDATE_LABEL_VALUE);
                        int position = bundle.getInt("position");
                        TaskItemModel objModel = (TaskItemModel) leadItemArray.get(position);
                        objModel.setLeadLabel(newLabel);
                        mAdapter.notifyItemChanged(position, objModel);
                        return;
                    }
                    return;
                } else if (resultCode == 0) {
                    Logger.m6e("PICKER", "Result Cancel");
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }*/

    @Override
    public void onResume() {
        super.onResume();
        setUpRecyclerView();
    }
}
