package com.example.syneotek001.androidtabsexample25042018.Adapter.dialog;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.Fragment.LeadListFragment;
import com.example.syneotek001.androidtabsexample25042018.R;

public class UpdateLeadLabelDialog extends AppCompatDialogFragment implements OnClickListener {
    private String currentLabel = "1";
    private int position;
    View view;

    public CardView cvLeadLabel;
    public ImageView ivClose, ivLabelClosing, ivLabelCold, ivLabelDead, ivLabelHot, ivLabelWarm;
    public LinearLayout llClosingLeads, llColdLeads, llDeadLeads, llHotLeads, llWarmLeads;
    private RelativeLayout mboundView0;
    public TextView tvLabelClosing, tvLabelCold, tvLabelDead, tvLabelHot, tvLabelWarm;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_update_lead_label, container, false);
        cvLeadLabel = view.findViewById(R.id.cvLeadLabel);
        ivClose = view.findViewById(R.id.ivClose);
        ivLabelClosing = view.findViewById(R.id.ivLabelClosing);
        ivLabelCold = view.findViewById(R.id.ivLabelCold);
        ivLabelDead = view.findViewById(R.id.ivLabelDead);
        ivLabelHot = view.findViewById(R.id.ivLabelHot);
        ivLabelWarm = view.findViewById(R.id.ivLabelWarm);
        llClosingLeads = view.findViewById(R.id.llClosingLeads);
        llColdLeads = view.findViewById(R.id.llColdLeads);
        llDeadLeads = view.findViewById(R.id.llDeadLeads);
        llHotLeads = view.findViewById(R.id.llHotLeads);
        llWarmLeads = view.findViewById(R.id.llWarmLeads);
        tvLabelClosing = view.findViewById(R.id.tvLabelClosing);
        tvLabelCold = view.findViewById(R.id.tvLabelCold);
        tvLabelDead = view.findViewById(R.id.tvLabelDead);
        tvLabelHot = view.findViewById(R.id.tvLabelHot);
        tvLabelWarm = view.findViewById(R.id.tvLabelWarm);
//            this.mBinding = (DialogUpdateLeadLabelBinding) DataBindingUtil.inflate(inflater, R.layout.dialog_update_lead_label, container, false);
//            this.view = this.mBinding.getRoot();

        prepareLayout(getArguments());
        setClickEvent();
        return view;
    }

    public void onStart() {
        super.onStart();
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));
    }

    private void prepareLayout(Bundle bundle) {
        if (bundle != null) {
            currentLabel = bundle.getString(LeadListFragment.BUNDLE_UPDATE_LABEL_VALUE);
            position = bundle.getInt("position");
        }
        String str = currentLabel;
        int i = 0;
        switch (str.hashCode()) {
            case 49:
                if (str.equals("1")) {
                    i = 0;
                    break;
                }
                break;
            case 50:
                if (str.equals("2")) {
                    i = 1;
                    break;
                }
                break;
            case 51:
                if (str.equals("3")) {
                    i = 2;
                    break;
                }
                break;
            case 52:
                if (str.equals("4")) {
                    i = 3;
                    break;
                }
                break;
            case 53:
                if (str.equals("5")) {
                    i = 4;
                    break;
                }
                break;
        }
        switch (i) {
            case 0:
                tvLabelHot.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_vector_check_black, 0);
                return;
            case 1:
                tvLabelWarm.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_vector_check_black, 0);
                return;
            case 2:
                tvLabelCold.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_vector_check_black, 0);
                return;
            case 3:
                tvLabelDead.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_vector_check_black, 0);
                return;
            case 4:
                tvLabelClosing.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_vector_check_black, 0);
                return;
            default:
                return;
        }
    }

    private void setClickEvent() {
        llHotLeads.setOnClickListener(this);
        llWarmLeads.setOnClickListener(this);
        llColdLeads.setOnClickListener(this);
        llDeadLeads.setOnClickListener(this);
        llClosingLeads.setOnClickListener(this);
        ivClose.setOnClickListener(this);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivClose:
                dismiss();
                break;
            case R.id.llClosingLeads:
                this.currentLabel = "5";
                break;
            case R.id.llColdLeads:
                this.currentLabel = "3";
                break;
            case R.id.llDeadLeads:
                this.currentLabel = "4";
                break;
            case R.id.llHotLeads:
                this.currentLabel = "1";
                break;
            case R.id.llWarmLeads:
                this.currentLabel = "2";
                break;
        }
        getTargetFragment().onActivityResult(getTargetRequestCode(), -1, new Intent().putExtra(LeadListFragment.BUNDLE_UPDATE_LABEL_VALUE, currentLabel).putExtra("position", position));
        dismiss();
    }
}
