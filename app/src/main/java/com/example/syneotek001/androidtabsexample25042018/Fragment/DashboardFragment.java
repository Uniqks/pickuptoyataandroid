package com.example.syneotek001.androidtabsexample25042018.Fragment;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.anychart.anychart.AnyChart;
import com.anychart.anychart.AnyChartView;
import com.anychart.anychart.DataEntry;
import com.anychart.anychart.EnumsAlign;
import com.anychart.anychart.LegendLayout;
import com.anychart.anychart.Pyramid;
import com.anychart.anychart.UiLegend;
import com.anychart.anychart.ValueDataEntry;
import com.example.syneotek001.androidtabsexample25042018.Adapter.DashboardItemAdapter;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.MainActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.Logger;
import com.example.syneotek001.androidtabsexample25042018.Utils.VerticalLabelView;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnRecyclerViewItemClickListener;
import com.example.syneotek001.androidtabsexample25042018.model.LineGraphDataModel;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.ArrayList;
import java.util.List;

public class DashboardFragment extends LineGraphBaseFragment implements OnClickListener {
    ArrayList<LineGraphDataModel> leadsEntry;
    RecyclerView recyclerView;
    private SlidingUpPanelLayout mLayout;

    public  LinearLayout llGraph;
    public  LineChart mChart;
    private long mDirtyFlags = -1;
    private  LinearLayout mboundView0;
    public  TextView tvMoreGraphs;
    public  TextView xAxisTitle;
    RelativeLayout rlLeadManager;
//    public VerticalLabelView yAxisTitle;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view=inflater.inflate(R.layout.fragment_analytics, container, false);
        recyclerView=view.findViewById(R.id.recyclerView);
        mLayout = (SlidingUpPanelLayout)view.findViewById(R.id.sliding_layout);

        tvMoreGraphs=view.findViewById(R.id.tvMoreGraphs);
        xAxisTitle=view.findViewById(R.id.xAxisTitle);
//        yAxisTitle=view.findViewById(R.id.yAxisTitle);
        llGraph=view.findViewById(R.id.llGraph);
        mChart=view.findViewById(R.id.mChart);
        rlLeadManager=view.findViewById(R.id.rlLeadManager);

        mLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                Log.d("onPanelSlide", "onPanelSlide, offset " + slideOffset);
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {
                Log.d("onPanelStateChanged", "onPanelStateChanged " + newState);
            }
        });
        mLayout.setFadeOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
            }
        });

        /*DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        int height = dm.heightPixels;
        mLayout.setPanelHeight(height/2);
*/
            setUpNavigationDrawer();
            setBinding(mChart);
            prepareGraphLayout(8, 800.0f);
            setClickEvents();
            setAnimation(view);


        AnyChartView anyChartView = view.findViewById(R.id.any_chart_view);
//        anyChartView.setProgressBar(view.findViewById(R.id.progress_bar));

        Pyramid pyramid = AnyChart.pyramid();

        List<DataEntry> data = new ArrayList<>();
        data.add(new ValueDataEntry("TV promotion", 6371664));
        data.add(new ValueDataEntry("Radio promotion", 7216301));
        data.add(new ValueDataEntry("Advertising leaflets", 1486621));
        data.add(new ValueDataEntry("Before advertising started", 1386622));

        pyramid.setData(data);

        UiLegend legend = pyramid.getLegend();
        legend.setEnabled(true);
        legend.setPosition("outside-right");
        legend.setItemsLayout(LegendLayout.VERTICAL);
        legend.setAlign(EnumsAlign.TOP);

        pyramid.setLabels(false);

        anyChartView.setChart(pyramid);


        return view;
    }


    private void setAnimation(View mview) {
        TextView tvLeadManager=mview.findViewById(R.id.tvLeadManager);
        TextView tvLeadManagerCount=mview.findViewById(R.id.tvLeadManagerCount);
        TextView tvTaskManager=mview.findViewById(R.id.tvTaskManager);
        TextView tvTaskManagerCount=mview.findViewById(R.id.tvTaskManagerCount);
        TextView tvOfflineLeads=mview.findViewById(R.id.tvOfflineLeads);
        TextView tvOfflineLeadsCount=mview.findViewById(R.id.tvOfflineLeadsCount);
        TextView tvLoremIpsum=mview.findViewById(R.id.tvLoremIpsum);
        TextView tvLoremIpsumCount=mview.findViewById(R.id.tvLoremIpsumCount);

        Animation a = AnimationUtils.loadAnimation(this.mContext, R.anim.slide_in_bottom_with_fade);
        a.reset();
        a.setStartOffset(200);
        tvLeadManager.clearAnimation();
        tvLeadManager.startAnimation(a);
        tvLeadManagerCount.clearAnimation();
        tvLeadManagerCount.startAnimation(a);
        tvTaskManager.clearAnimation();
        tvTaskManager.startAnimation(a);
        tvTaskManagerCount.clearAnimation();
        tvTaskManagerCount.startAnimation(a);
        tvOfflineLeads.clearAnimation();
        tvOfflineLeads.startAnimation(a);
        tvOfflineLeadsCount.clearAnimation();
        tvOfflineLeadsCount.startAnimation(a);
        tvLoremIpsum.clearAnimation();
        tvLoremIpsum.startAnimation(a);
        tvLoremIpsumCount.clearAnimation();
        tvLoremIpsumCount.startAnimation(a);
    }

    /*private void setClickEvents() {
        this.mBinding.data.rlLeadManager.setOnClickListener(this);
        this.mBinding.data.rlLoremIpsum.setOnClickListener(this);
        this.mBinding.data.rlOfflineLeads.setOnClickListener(this);
        this.mBinding.data.rlTaskManager.setOnClickListener(this);
        this.mBinding.chart.tvMoreGraphs.setOnClickListener(this);
    }*/

    private void setClickEvents(){
        rlLeadManager.setOnClickListener(this);
        tvMoreGraphs.setOnClickListener(this);
    }

    private void setUpNavigationDrawer() {
        final ArrayList<String> dashboardItemArray = new ArrayList();
        dashboardItemArray.add(getResources().getString(R.string.account));
        dashboardItemArray.add(getResources().getString(R.string.dashboard));
        dashboardItemArray.add(getResources().getString(R.string.message));
        dashboardItemArray.add(getResources().getString(R.string.email));
        dashboardItemArray.add(getResources().getString(R.string.help_center));
        dashboardItemArray.add(getResources().getString(R.string.announcement));
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        DashboardItemAdapter ndAdapter = new DashboardItemAdapter(getActivity(), dashboardItemArray, new OnRecyclerViewItemClickListener() {
            public void onItemClicked(int position) {
                Logger.m6e("Selected Item", (String) dashboardItemArray.get(position));
                if (position == 3) {
                    ((HomeActivity)getActivity()).replaceFragment(new AnnouncementFragment());
                } else if (position == 2) {
                    ((HomeActivity)getActivity()).replaceFragment(new HelpCenterFragment());
                } else if (position == 1) {
                    ((HomeActivity)getActivity()).replaceFragment(new EmailInboxListFragment());
                } else if (position == 0) {
                    ((HomeActivity)getActivity()).replaceFragment(new ChatListHolderFragment());
                }
            }
        });
        recyclerView.setAdapter(ndAdapter);
        ndAdapter.setLastSelectedPosition(1);
    }

    private void prepareGraphLayout(int count, float range) {
        ArrayList<Entry> newLeads = new ArrayList<>();
        ArrayList<Entry> followUpLeads = new ArrayList<>();
        ArrayList<Entry> underContractLeads = new ArrayList<>();
        ArrayList<Entry> deliveryLeads = new ArrayList<>();
        ArrayList<Entry> soldLeads = new ArrayList<>();
        ArrayList<Entry> inactiveLeads = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            newLeads.add(new Entry((float) i, ((float) (Math.random() * ((double) range))) + 3.0f, getResources().getDrawable(R.drawable.ic_chart_marker_yellow)));
            followUpLeads.add(new Entry((float) i, 20.0f + (((float) (Math.random() * ((double) range))) + 3.0f), getResources().getDrawable(R.drawable.ic_chart_marker_orange)));
            underContractLeads.add(new Entry((float) i, 50.0f + (((float) (Math.random() * ((double) range))) + 3.0f), getResources().getDrawable(R.drawable.ic_chart_marker_sky)));
            deliveryLeads.add(new Entry((float) i, 30.0f + (((float) (Math.random() * ((double) range))) + 3.0f), getResources().getDrawable(R.drawable.ic_chart_marker_green)));
            soldLeads.add(new Entry((float) i, 10.0f + (((float) (Math.random() * ((double) range))) + 3.0f), getResources().getDrawable(R.drawable.ic_chart_marker_pink)));
            inactiveLeads.add(new Entry((float) i, 25.0f + (((float) (Math.random() * ((double) range))) + 3.0f), getResources().getDrawable(R.drawable.ic_chart_marker_red)));
        }
        this.leadsEntry = new ArrayList<>();
        this.leadsEntry.add(new LineGraphDataModel("New", ContextCompat.getColor(this.mContext, R.color.yellow_opacity_80), ContextCompat.getDrawable(this.mContext, R.drawable.fade_yellow), ContextCompat.getColor(this.mContext, R.color.yellow_opacity_50), newLeads));
        this.leadsEntry.add(new LineGraphDataModel("FollowUp", ContextCompat.getColor(this.mContext, R.color.orange_opacity_80), ContextCompat.getDrawable(this.mContext, R.drawable.fade_orange), ContextCompat.getColor(this.mContext, R.color.orange_opacity_50), followUpLeads));
        this.leadsEntry.add(new LineGraphDataModel("UnderContract", ContextCompat.getColor(this.mContext, R.color.sky_opacity_80), ContextCompat.getDrawable(this.mContext, R.drawable.fade_sky), ContextCompat.getColor(this.mContext, R.color.sky_opacity_50), underContractLeads));
        this.leadsEntry.add(new LineGraphDataModel("Delivery", ContextCompat.getColor(this.mContext, R.color.green_opacity_80), ContextCompat.getDrawable(this.mContext, R.drawable.fade_green), ContextCompat.getColor(this.mContext, R.color.green_opacity_50), deliveryLeads));
        this.leadsEntry.add(new LineGraphDataModel("Sold", ContextCompat.getColor(this.mContext, R.color.pink_opacity_80), ContextCompat.getDrawable(this.mContext, R.drawable.fade_pink), ContextCompat.getColor(this.mContext, R.color.pink_opacity_50), soldLeads));
        this.leadsEntry.add(new LineGraphDataModel("Inactive", ContextCompat.getColor(this.mContext, R.color.red_opacity_80), ContextCompat.getDrawable(this.mContext, R.drawable.fade_red), ContextCompat.getColor(this.mContext, R.color.red_opacity_50), inactiveLeads));
        setUpChart(2010, count + 2010, this.leadsEntry);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rlLeadManager:
                ((HomeActivity)getActivity()).replaceFragment(new LeadListHolderFragment());
                return;
            case R.id.tvMoreGraphs:
                ((HomeActivity)getActivity()).replaceFragment(new AnalyticsHolderFragment());
                return;
            default:
                return;
        }
    }
}
