package com.example.syneotek001.androidtabsexample25042018;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;


public class BaseActivity extends AppCompatActivity {
    protected boolean shouldPerformDispatchTouch = true;
    TextView title;
    public Toolbar toolbar;
    class C05641 implements OnClickListener {
        C05641() {
        }
        public void onClick(View view) {
            BaseActivity.this.onBackPressed();
        }
    }
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    protected void onStart() {
        super.onStart();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    protected void onDestroy() {
        super.onDestroy();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    public void setUpToolbar(String strTitle) {
        setUpToolbarWithBackArrow(strTitle, false);
    }

    public void setUpToolbarWithBackArrow(String strTitle) {
        setUpToolbarWithBackArrow(strTitle, true);
    }

    private void setUpToolbarWithBackArrow(String strTitle, boolean isBackArrow) {
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(this.toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            if (isBackArrow) {
                actionBar.setHomeAsUpIndicator((int) R.drawable.ic_vector_arrow_back);
            }
        }
        this.toolbar.setNavigationOnClickListener(new C05641());
        this.title = (TextView) this.toolbar.findViewById(R.id.tvTitle);
        this.title.setText(strTitle);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case 16908332:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean dispatchTouchEvent(MotionEvent event) {
        try {
            View view = getCurrentFocus();
            boolean ret = super.dispatchTouchEvent(event);
            if (this.shouldPerformDispatchTouch && (view instanceof EditText)) {
                View w = getCurrentFocus();
                int[] scrCords = new int[2];
                if (w != null) {
                    w.getLocationOnScreen(scrCords);
                    float x = (event.getRawX() + ((float) w.getLeft())) - ((float) scrCords[0]);
                    float y = (event.getRawY() + ((float) w.getTop())) - ((float) scrCords[1]);
                    if (event.getAction() == 1 && (x < ((float) w.getLeft()) || x >= ((float) w.getRight()) || y < ((float) w.getTop()) || y > ((float) w.getBottom()))) {
                        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    }
                }
            }
            return ret;
        } catch (Exception e) {
            return false;
        }
    }
}
