package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.interfaces.OnLeadItemViewClickListener;
import com.example.syneotek001.androidtabsexample25042018.model.TaskItemModel;

import java.util.ArrayList;
import java.util.Iterator;


public class EmailTemplateItemAdapter extends RecyclerView.Adapter<EmailTemplateItemAdapter.MyViewHolder> implements OnClickListener {
    private int lastSelectedPosition = 1;
    private Context mContext;
    private ArrayList<TaskItemModel> mData;
    private OnLeadItemViewClickListener onItemClickListener;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivShare:
                if (EmailTemplateItemAdapter.this.onItemClickListener != null) {
                    EmailTemplateItemAdapter.this.onItemClickListener.onLeadDetailIconClicked(lastSelectedPosition);
                    return;
                }
                return;
            case R.id.ivLeadLabel:
                if (EmailTemplateItemAdapter.this.onItemClickListener != null) {
                    EmailTemplateItemAdapter.this.onItemClickListener.onLeadLabelClicked(lastSelectedPosition);
                    return;
                }
                return;
            case R.id.ivEdit:
                if (EmailTemplateItemAdapter.this.onItemClickListener != null) {
                    EmailTemplateItemAdapter.this.onItemClickListener.onLeadEmailIconClicked(lastSelectedPosition);
                    return;
                }
                return;
            case R.id.ivSchedule:
                if (EmailTemplateItemAdapter.this.onItemClickListener != null) {
                    EmailTemplateItemAdapter.this.onItemClickListener.onLeadScheduleIconClicked(lastSelectedPosition);
                    return;
                }
                return;
            default:
                return;
        }
    }

    class MyViewHolder extends ViewHolder implements OnClickListener {

        CardView cvLead;
        ImageView ivEdit, ivDelete, ivShare;
        TextView  tvHeadTaskTitle;

        MyViewHolder(View view) {
            super(view);
            cvLead = view.findViewById(R.id.cvLead);
            ivEdit = view.findViewById(R.id.ivEdit);
            ivDelete = view.findViewById(R.id.ivDelete);
            ivShare = view.findViewById(R.id.ivShare);
            tvHeadTaskTitle = view.findViewById(R.id.tvHeadTaskTitle);

        }

        @Override
        public void onClick(View v) {

        }
    }

    public EmailTemplateItemAdapter(Context context, ArrayList<TaskItemModel> list, OnLeadItemViewClickListener onItemClickListener) {
        this.mContext = context;
        this.mData = list;
        this.onItemClickListener = onItemClickListener;
    }

    public int getItemViewType(int position) {
        return 1;
    }


    @NonNull
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_email_template, parent, false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        TaskItemModel mLeadItem = (TaskItemModel) mData.get(position);
        holder.tvHeadTaskTitle.setText(mLeadItem.getTask_title());
        holder.itemView.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                if (onItemClickListener != null) {
                    TaskItemModel mLeadItem = (TaskItemModel) mData.get(position);
                    mLeadItem.setSelected(!mLeadItem.isSelected());
                    notifyItemChanged(position, mLeadItem);
                }
            }
        });
        holder.ivShare.setOnClickListener(this);
        holder.ivEdit.setOnClickListener(this);
        holder.ivDelete.setOnClickListener(this);
    }


    public int getItemCount() {
        return mData.size();
    }

    public void setLastSelectedPosition(int position) {
        this.lastSelectedPosition = position;
    }
}
