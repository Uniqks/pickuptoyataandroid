package com.example.syneotek001.androidtabsexample25042018.Adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.example.syneotek001.androidtabsexample25042018.Fragment.ChatMessagesFragment;
import com.example.syneotek001.androidtabsexample25042018.HomeActivity;
import com.example.syneotek001.androidtabsexample25042018.MainActivity;
import com.example.syneotek001.androidtabsexample25042018.R;
import com.example.syneotek001.androidtabsexample25042018.Utils.GlideUtils;
import com.example.syneotek001.androidtabsexample25042018.model.ChatModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ChatItemAdapter extends RecyclerView.Adapter<ChatItemAdapter.MyViewHolder> {
    private GlideUtils glideUtils;
    private Context mContext;
    private List<ChatModel> mData;

    class MyViewHolder extends ViewHolder {
        ImageView ivOnlineIndicator, ivUserImage;
        RelativeLayout layoutChatImage;
        TextView tvDateTime, tvUnreadMessage, tvUserLetter, tvUserName;

        MyViewHolder(View view) {
            super(view);
            ivOnlineIndicator = view.findViewById(R.id.ivOnlineIndicator);
            ivUserImage = view.findViewById(R.id.ivUserImage);
            layoutChatImage = view.findViewById(R.id.layoutChatImage);
            tvDateTime = view.findViewById(R.id.tvDateTime);
            tvUnreadMessage = view.findViewById(R.id.tvUnreadMessage);
            tvUserLetter = view.findViewById(R.id.tvUserLetter);
            tvUserName = view.findViewById(R.id.tvUserName);
        }
    }

    public ChatItemAdapter(Context context, ArrayList<ChatModel> list) {
        this.mContext = context;
        this.mData = list;
        this.glideUtils = new GlideUtils(mContext);
    }

    public int getItemViewType(int position) {
        return 1;
    }

    @NonNull
    public ChatItemAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_chat, parent, false);

        return new ChatItemAdapter.MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        int i;
        ChatModel mChatModel = (ChatModel) this.mData.get(position);
        holder.tvUserName.setText(mChatModel.getName());
        ImageView imageView = holder.ivOnlineIndicator;
        if (mChatModel.isOnline()) {
            i = 0;
        } else {
            i = 8;
        }
        imageView.setVisibility(i);
        if (mChatModel.getImage() == null || mChatModel.getImage().length() <= 0) {
            String title = mChatModel.getName().toUpperCase();
            if (title.length() > 0) {
                holder.tvUserLetter.setText(title.substring(0, 1).toUpperCase());
            } else {
                holder.tvUserLetter.setText("T");
            }
            holder.tvUserLetter.setVisibility(View.VISIBLE);
            glideUtils.loadImageCircular("", holder.ivUserImage);
        } else {
            glideUtils.loadImageCircular(mChatModel.getImage(), holder.ivUserImage);
            holder.tvUserLetter.setVisibility(View.GONE);
        }
        if (mChatModel.getUnreadCount() > 0) {
            holder.tvUnreadMessage.setText(this.mContext.getResources().getString(mChatModel.getUnreadCount() > 1 ? R.string._unread_messages : R.string._unread_message, new Object[]{String.valueOf(mChatModel.getUnreadCount())}));
            holder.tvUnreadMessage.setVisibility(View.VISIBLE);
            return;
        }
        holder.tvUnreadMessage.setVisibility(View.GONE);

        holder.itemView.setOnClickListener(new OnClickListener() {
            public void onClick(View view) {
                ChatMessagesFragment chatMessages = new ChatMessagesFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable(ChatMessagesFragment.BUNDLE_CHAT_FRIEND_MODEL, (Serializable) mData.get(position));
                chatMessages.setArguments(bundle);
                ((HomeActivity) mContext).replaceFragment(chatMessages);
//                ((MainActivity) ChatItemAdapter.this.mContext).pushFragment(((MainActivity) ChatItemAdapter.this.mContext).mCurrentTab, chatMessages, true, true);
            }
        });
    }

    public int getItemCount() {
        return this.mData.size();
    }
}
