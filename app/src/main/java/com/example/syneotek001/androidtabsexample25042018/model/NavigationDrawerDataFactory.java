package com.example.syneotek001.androidtabsexample25042018.model;


import com.example.syneotek001.androidtabsexample25042018.Adapter.NavigationDrawerChildModel;
import com.example.syneotek001.androidtabsexample25042018.Adapter.NavigationDrawerParentModel;
import com.example.syneotek001.androidtabsexample25042018.R;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public class NavigationDrawerDataFactory {
    public static final String NAV_MENU_CAR_BUILDER = "Car Builder";
    public static final String NAV_MENU_CONTACT_LIST = "Contact List";
    private static final String NAV_MENU_DEAL_MANAGER = "Deal Manager";
    public static final String NAV_MENU_DEAL_MANAGER_DEAL_BUILDER = "Deal Builder";
    public static final String NAV_MENU_DEAL_MANAGER_DEAL_MANAGER = "Deal Manager";
    private static final String NAV_MENU_EMAIL = "Email";
    public static final String NAV_MENU_EMAIL_INBOX = "Inbox";
    public static final String NAV_MENU_EMAIL_MARKETING = "Email Marketing";
    public static final String NAV_MENU_EMAIL_TEMPLATE = "Email Template";
    public static final String NAV_MENU_HELP = "Help";
    private static final String NAV_MENU_LEAD_MANAGER = "Lead Manager";
    public static final String NAV_MENU_LEAD_MANAGER_ACTIVE_LEADS = "Active Leads";
    public static final String NAV_MENU_LEAD_MANAGER_ADD_NEW_LEAD = "Add New Lead";
    public static final String NAV_MENU_LEAD_MANAGER_IN_ACTIVE_LEADS = "In Active Leads";
    public static final String NAV_MENU_LEAD_MANAGER_SOLD_LEADS = "Sold Leads";
    public static final String NAV_MENU_LOGOUT = "Logout";

    public static List<NavigationDrawerParentModel> makeGenres() {
        return Arrays.asList(new NavigationDrawerParentModel[]{makeParentLeadManager(), makeParentEmail(), makeParentDealManager(), makeParentContactList(), makeParentCarBuilder(), makeParentHelp(), makeParentLogout()});
    }

    private static NavigationDrawerParentModel makeParentLeadManager() {
        return new NavigationDrawerParentModel(NAV_MENU_LEAD_MANAGER, makeChildLeadManager(), R.drawable.ic_vector_drawer_lead_manager_black);
    }

    private static List<NavigationDrawerChildModel> makeChildLeadManager() {
        NavigationDrawerChildModel activeLeads = new NavigationDrawerChildModel(NAV_MENU_LEAD_MANAGER_ACTIVE_LEADS, true);
        NavigationDrawerChildModel soldLeads = new NavigationDrawerChildModel(NAV_MENU_LEAD_MANAGER_SOLD_LEADS, false);
        NavigationDrawerChildModel inActiveLeads = new NavigationDrawerChildModel(NAV_MENU_LEAD_MANAGER_IN_ACTIVE_LEADS, false);
        NavigationDrawerChildModel newLeads = new NavigationDrawerChildModel(NAV_MENU_LEAD_MANAGER_ADD_NEW_LEAD, true);
        return Arrays.asList(new NavigationDrawerChildModel[]{activeLeads, soldLeads, inActiveLeads, newLeads});
    }

    private static NavigationDrawerParentModel makeParentEmail() {
        return new NavigationDrawerParentModel(NAV_MENU_EMAIL, makeChildEmail(), R.drawable.ic_vector_drawer_email_black);
    }

    private static List<NavigationDrawerChildModel> makeChildEmail() {
        NavigationDrawerChildModel inbox = new NavigationDrawerChildModel(NAV_MENU_EMAIL_INBOX, true);
        NavigationDrawerChildModel emailTemplate = new NavigationDrawerChildModel(NAV_MENU_EMAIL_TEMPLATE, true);
        NavigationDrawerChildModel emailMarketing = new NavigationDrawerChildModel(NAV_MENU_EMAIL_MARKETING, false);
        return Arrays.asList(new NavigationDrawerChildModel[]{inbox, emailTemplate, emailMarketing});
    }

    private static NavigationDrawerParentModel makeParentDealManager() {
        return new NavigationDrawerParentModel("Deal Manager", makeChildDealManager(), R.drawable.ic_vector_drawer_deal_manager_black);
    }

    private static List<NavigationDrawerChildModel> makeChildDealManager() {
        NavigationDrawerChildModel dealManager = new NavigationDrawerChildModel("Deal Manager", true);
        NavigationDrawerChildModel dealBuilder = new NavigationDrawerChildModel(NAV_MENU_DEAL_MANAGER_DEAL_BUILDER, true);
        return Arrays.asList(new NavigationDrawerChildModel[]{dealManager, dealBuilder});
    }

    private static NavigationDrawerParentModel makeParentContactList() {
        return new NavigationDrawerParentModel(NAV_MENU_CONTACT_LIST, makeChildContactList(), R.drawable.ic_vector_drawer_contact_list_black);
    }

    private static List<NavigationDrawerChildModel> makeChildContactList() {
        return Collections.emptyList();
    }

    private static NavigationDrawerParentModel makeParentCarBuilder() {
        return new NavigationDrawerParentModel(NAV_MENU_CAR_BUILDER, makeChildCarBuilder(), R.drawable.ic_vector_drawer_car_black);
    }

    private static List<NavigationDrawerChildModel> makeChildCarBuilder() {
        return Collections.emptyList();
    }

    private static NavigationDrawerParentModel makeParentHelp() {
        return new NavigationDrawerParentModel(NAV_MENU_HELP, makeChildHelp(), R.drawable.ic_vector_drawer_help_black);
    }

    private static List<NavigationDrawerChildModel> makeChildHelp() {
        return Collections.emptyList();
    }

    private static NavigationDrawerParentModel makeParentLogout() {
        return new NavigationDrawerParentModel(NAV_MENU_LOGOUT, makeChildLogout(), R.drawable.ic_vector_drawer_logout_black);
    }

    private static List<NavigationDrawerChildModel> makeChildLogout() {
        return Collections.emptyList();
    }
}
